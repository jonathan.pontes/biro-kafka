package com.kafka.birocadastro.service;

import com.kafka.birocadastro.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void cadastrarEmpresa(Empresa empresa) {

        producer.send("spec2-jonathan-roberto-2", empresa);
        System.out.println("Nova empresa postada na fila: "
                + "\nNome: " + empresa.getNome()
                + "\nCNPJ: " + empresa.getCnpj());
    }
}
