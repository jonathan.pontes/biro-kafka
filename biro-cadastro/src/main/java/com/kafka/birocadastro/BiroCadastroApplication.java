package com.kafka.birocadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiroCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiroCadastroApplication.class, args);
	}

}
