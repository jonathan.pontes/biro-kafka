package com.kafka.birocadastro.controller;

import com.kafka.birocadastro.model.Empresa;
import com.kafka.birocadastro.service.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaProducer empresaProducer;

    @PostMapping("/cadastro")
    public void cadastrar(@RequestBody Empresa empresa) {
        empresaProducer.cadastrarEmpresa(empresa);
    }
}
