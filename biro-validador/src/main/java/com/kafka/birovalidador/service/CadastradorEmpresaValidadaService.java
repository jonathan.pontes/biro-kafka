package com.kafka.birovalidador.service;

import com.kafka.birocadastro.model.Empresa;
import com.kafka.birovalidador.apigoverno.model.ApiGoverno;
import com.kafka.birovalidador.apigoverno.service.ApiGovernoService;
import com.kafka.birovalidador.model.EmpresaValidada;
import com.kafka.birovalidador.producer.ValidacaoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CadastradorEmpresaValidadaService {

    @Autowired
    ApiGovernoService apiGovernoService;

    @Autowired
    private ValidacaoProducer validacaoProducer;

    public void cadastrarEmpresasValidadas(Empresa empresa) {
        String cnpjSemFormatacao = getCnpjSemFormatacao(empresa);

        ApiGoverno apiGoverno = apiGovernoService.obterCapitalSocial(cnpjSemFormatacao);

        EmpresaValidada empresaValidada = getEmpresaValidada(empresa);

        if(apiGoverno.getCapitalSocial().compareTo(new BigDecimal(1000.0)) == 1){
            empresaValidada.setValido(true);
        }else{
            empresaValidada.setValido(false);
        }

        enviarParaFilaEmpresaValidada(empresaValidada);
    }

    private void enviarParaFilaEmpresaValidada(EmpresaValidada empresaValidada) {
        validacaoProducer.enviarParaAFilaUmaEmpresaValidada(empresaValidada);
    }

    private String getCnpjSemFormatacao(Empresa empresa) {
        return empresa.getCnpj().replaceAll("[./-]", "");
    }

    private EmpresaValidada getEmpresaValidada(Empresa empresa) {
        EmpresaValidada empresaValidada = new EmpresaValidada();
        empresaValidada.setCnpj(empresa.getCnpj());
        empresaValidada.setNome(empresa.getNome());
        return empresaValidada;
    }
}
