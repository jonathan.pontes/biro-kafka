package com.kafka.birovalidador.consumer;

import com.kafka.birocadastro.model.Empresa;
import com.kafka.birovalidador.service.CadastradorEmpresaValidadaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    CadastradorEmpresaValidadaService cadastrador;

    @KafkaListener(topics = "spec2-jonathan-roberto-2", groupId = "novo")
    public void receber(@Payload Empresa empresa) {

        System.out.println("Empresa nova cadastrada "
                + "\nNome: " + empresa.getCnpj()
                + "\nPorta: " + empresa.getNome());

        cadastrador.cadastrarEmpresasValidadas(empresa);
    }

}
