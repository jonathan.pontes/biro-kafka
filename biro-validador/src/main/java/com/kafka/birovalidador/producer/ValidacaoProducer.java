package com.kafka.birovalidador.producer;

import com.kafka.birovalidador.model.EmpresaValidada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoProducer {

    @Autowired
    private KafkaTemplate<String, EmpresaValidada> producer;

    public void enviarParaAFilaUmaEmpresaValidada(EmpresaValidada empresaValidada) {

        producer.send("spec2-jonathan-roberto-3", empresaValidada);
        System.out.println("Nova empresa validada postada na fila: "
                + "\nNome: " + empresaValidada.getNome()
                + "\nCNPJ: " + empresaValidada.getCnpj()
                + "\nValidação: " + empresaValidada.getValido());
    }
}
