package com.kafka.birocsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiroCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiroCsvApplication.class, args);
	}

}
