package com.kafka.birocsv.consumer;

import com.kafka.birovalidador.model.EmpresaValidada;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class EmpresaValidadaConsumer {

    @KafkaListener(topics = "spec2-jonathan-roberto-3", groupId = "validada")
    public void receber(@Payload EmpresaValidada empresaValidada) {

        try {
            FileWriter arquivo = null;
            if(empresaValidada.getValido()){
                arquivo = new FileWriter("/home/a2/kafka-consumer/empresa-validada/empresa-ok.txt", true);
            }else{
                arquivo = new FileWriter("/home/a2/kafka-consumer/empresa-validada/empresa-nao-ok.txt", true);
            }

            PrintWriter gravarArquivo = new PrintWriter(arquivo);

            gravarArquivo.append("\n"
                    + empresaValidada.getNome() + ";"
                    + empresaValidada.getCnpj() + ";"
                    + empresaValidada.getValido());

            gravarArquivo.flush();
            arquivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Nova Empresa validada: "
                + "\nNome: " + empresaValidada.getNome()
                + "\nCnpj: " + empresaValidada.getCnpj()
                + "\nValidação: " + empresaValidada.getValido());

    }


}
